import pandas as pd
from camelot import read_pdf
from difflib import get_close_matches


price_file = read_pdf("./lista_precos - lista_precos.pdf", pages="1-end")

dict_of_prices = {}

for page in price_file:
    for item in page.df.to_dict("split")["data"]:
        dict_of_prices[item[1]] = {"supplier": item[0], "price": item[2]}

dict_of_prices.pop("Descrição")


with open('./PEDIDOS.txt', 'r') as r:
    incoming_orders = ("|".join(r.read().splitlines())).split('||')
    incoming_orders_lines = [incoming_order.split(
        '|') for incoming_order in incoming_orders]
    dict_cast_orders_lines = dict([("".join([letter for letter in element[0] if letter == " " or letter.isalpha(
    )]).strip(), element[1:]) for element in incoming_orders_lines])


final_table_rows = []

for client, products_list in dict_cast_orders_lines.items():
    for product in products_list:
        quantity_values = [
            number for number in product if number.isnumeric()]

        amount = int(quantity_values[0]) if quantity_values else 1

        product_name = " ".join(
            letter for letter in product.split(" ") if letter.isalpha())

        matches_products_name_list = get_close_matches(
            product_name, dict_of_prices.keys(), cutoff=0.3)

        for item in matches_products_name_list:
            if product_name[:2].lower() not in item.lower():
                matches_products_name_list.remove(item)

        product_found = matches_products_name_list[0] if matches_products_name_list else ""

        supplier = dict_of_prices.get(
            product_found, {"supplier": ""})["supplier"]

        price = float(dict_of_prices.get(
            product_found, {"price": "0"})["price"].replace(',', '.'))

        total_value = amount*price

        final_table_rows.append(
            [client, product_name, product_found, amount, price, total_value])

df = pd.DataFrame(final_table_rows, columns=[
    "Nome do Cliente", "Produto Pedido", "Produto Encontrado", "Quantidade", "Valor Unitário", "Valor Total"])
df.to_excel("Jacyr_Organics_Orders.xlsx")

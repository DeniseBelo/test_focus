# Test Focus Project

This project is a program where the client receives a list of orders in txt format and the list is converted in an excel file. IMPORTANT: The incoming text must be without spaces between the name and the products. It must be in a block format to work properly in this program.

## How to get started:

You need to clone this project in thus gitlab url:

```
https://gitlab.com/DeniseBelo/test_focus
```

Then you create a directory in your machine and use a command line to clone it:

```
git clone https://gitlab.com/DeniseBelo/test_focus.git
```

Enter the created file directory and install python.

```
python -m venv venv --upgrade-deps
```

Enter the virtual environment:

```
source venv/bin/activate
```

```
pip install -r requirements.txt
```

Ok, now you have all the dependencies you need to run the application. Just execute a command line to run the file and then open the xlsx file generated.

```
python main.py
```

##Technologies:

- Python;
- Pandas;
- Camelot.
